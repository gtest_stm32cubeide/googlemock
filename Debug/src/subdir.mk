################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../src/gmock-cardinalities.cc \
../src/gmock-internal-utils.cc \
../src/gmock-matchers.cc \
../src/gmock-spec-builders.cc \
../src/gmock.cc \
../src/gmock_main.cc 

CC_DEPS += \
./src/gmock-cardinalities.d \
./src/gmock-internal-utils.d \
./src/gmock-matchers.d \
./src/gmock-spec-builders.d \
./src/gmock.d \
./src/gmock_main.d 

OBJS += \
./src/gmock-cardinalities.o \
./src/gmock-internal-utils.o \
./src/gmock-matchers.o \
./src/gmock-spec-builders.o \
./src/gmock.o \
./src/gmock_main.o 


# Each subdirectory must supply rules for building sources it contributes
src/gmock-cardinalities.o: ../src/gmock-cardinalities.cc
	arm-none-eabi-g++ "$<" -mcpu=cortex-m4 -std=gnu++14 -g3 '-DMBEDTLS_CONFIG_FILE="mbedtls_config.h"' -DUSE_HAL_DRIVER '-DLWIP_DEBUG=1' -DDEBUG -DSTM32F407xx '-DUNIT_TESTING=0' -c -I"C:/Users/Tim/Documents/Projecten/Prive/SCPI-Remote/googletest" -I../include -I"C:/Users/Tim/Documents/Projecten/Prive/SCPI-Remote/googletest/include" -O0 -ffunction-sections -fdata-sections -fno-exceptions -fno-rtti -fno-threadsafe-statics -fno-use-cxa-atexit -Wall -fstack-usage -MMD -MP -MF"src/gmock-cardinalities.d" -MT"$@"  -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
src/gmock-internal-utils.o: ../src/gmock-internal-utils.cc
	arm-none-eabi-g++ "$<" -mcpu=cortex-m4 -std=gnu++14 -g3 '-DMBEDTLS_CONFIG_FILE="mbedtls_config.h"' -DUSE_HAL_DRIVER '-DLWIP_DEBUG=1' -DDEBUG -DSTM32F407xx '-DUNIT_TESTING=0' -c -I"C:/Users/Tim/Documents/Projecten/Prive/SCPI-Remote/googletest" -I../include -I"C:/Users/Tim/Documents/Projecten/Prive/SCPI-Remote/googletest/include" -O0 -ffunction-sections -fdata-sections -fno-exceptions -fno-rtti -fno-threadsafe-statics -fno-use-cxa-atexit -Wall -fstack-usage -MMD -MP -MF"src/gmock-internal-utils.d" -MT"$@"  -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
src/gmock-matchers.o: ../src/gmock-matchers.cc
	arm-none-eabi-g++ "$<" -mcpu=cortex-m4 -std=gnu++14 -g3 '-DMBEDTLS_CONFIG_FILE="mbedtls_config.h"' -DUSE_HAL_DRIVER '-DLWIP_DEBUG=1' -DDEBUG -DSTM32F407xx '-DUNIT_TESTING=0' -c -I"C:/Users/Tim/Documents/Projecten/Prive/SCPI-Remote/googletest" -I../include -I"C:/Users/Tim/Documents/Projecten/Prive/SCPI-Remote/googletest/include" -O0 -ffunction-sections -fdata-sections -fno-exceptions -fno-rtti -fno-threadsafe-statics -fno-use-cxa-atexit -Wall -fstack-usage -MMD -MP -MF"src/gmock-matchers.d" -MT"$@"  -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
src/gmock-spec-builders.o: ../src/gmock-spec-builders.cc
	arm-none-eabi-g++ "$<" -mcpu=cortex-m4 -std=gnu++14 -g3 '-DMBEDTLS_CONFIG_FILE="mbedtls_config.h"' -DUSE_HAL_DRIVER '-DLWIP_DEBUG=1' -DDEBUG -DSTM32F407xx '-DUNIT_TESTING=0' -c -I"C:/Users/Tim/Documents/Projecten/Prive/SCPI-Remote/googletest" -I../include -I"C:/Users/Tim/Documents/Projecten/Prive/SCPI-Remote/googletest/include" -O0 -ffunction-sections -fdata-sections -fno-exceptions -fno-rtti -fno-threadsafe-statics -fno-use-cxa-atexit -Wall -fstack-usage -MMD -MP -MF"src/gmock-spec-builders.d" -MT"$@"  -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
src/gmock.o: ../src/gmock.cc
	arm-none-eabi-g++ "$<" -mcpu=cortex-m4 -std=gnu++14 -g3 '-DMBEDTLS_CONFIG_FILE="mbedtls_config.h"' -DUSE_HAL_DRIVER '-DLWIP_DEBUG=1' -DDEBUG -DSTM32F407xx '-DUNIT_TESTING=0' -c -I"C:/Users/Tim/Documents/Projecten/Prive/SCPI-Remote/googletest" -I../include -I"C:/Users/Tim/Documents/Projecten/Prive/SCPI-Remote/googletest/include" -O0 -ffunction-sections -fdata-sections -fno-exceptions -fno-rtti -fno-threadsafe-statics -fno-use-cxa-atexit -Wall -fstack-usage -MMD -MP -MF"src/gmock.d" -MT"$@"  -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
src/gmock_main.o: ../src/gmock_main.cc
	arm-none-eabi-g++ "$<" -mcpu=cortex-m4 -std=gnu++14 -g3 '-DMBEDTLS_CONFIG_FILE="mbedtls_config.h"' -DUSE_HAL_DRIVER '-DLWIP_DEBUG=1' -DDEBUG -DSTM32F407xx '-DUNIT_TESTING=0' -c -I"C:/Users/Tim/Documents/Projecten/Prive/SCPI-Remote/googletest" -I../include -I"C:/Users/Tim/Documents/Projecten/Prive/SCPI-Remote/googletest/include" -O0 -ffunction-sections -fdata-sections -fno-exceptions -fno-rtti -fno-threadsafe-statics -fno-use-cxa-atexit -Wall -fstack-usage -MMD -MP -MF"src/gmock_main.d" -MT"$@"  -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

